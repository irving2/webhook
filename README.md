# Really simple webhook

## Deploy apps to staging

```
source env/bin/activate
```

1. pip3 install -r requirements.txt
2. open themes.json file and configure your staging folders

- theme = ID
- path = full path to app folder
- deployscript = script to execute in path

````
{
  "name":"test-local",
  "path": "/Users/idelacruz/Projects/work/test/test",
  "commands": "git pull && npm run build"
}
````
3. open .env file and add token

## Sample curl to deploy 'test' project
```
curl -X POST 'http://127.0.0.1:5002/webhook/test' \
-H 'X-Gitlab-Token: abcdefghijkmnopqrstuvwxyz' 
```

4. run app 
```
python3 app.py 
or
pm2 start app.py --name webhook --interpreter python3
```