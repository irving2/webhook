#GITLAB WEBHOOK

import os
import json
from flask import Flask, request, abort, jsonify
from dotenv import load_dotenv

load_dotenv()
SECRET_TOKEN = os.getenv("TOKEN")
PORT = os.getenv("PORT")
APP_PATH = os.path.dirname(os.path.realpath(__file__))
BRANCH = "develop"

app = Flask(__name__)

PROJECTS = []
PATHS = []
DEPLOYMENTSCRIPT = []

with open('projects.json') as json_file:
    data = json.load(json_file)
    for item in data['projects']:
        PROJECTS.append(item['name'])
        PATHS.append(item['path'])
        DEPLOYMENTSCRIPT.append(item['commands'])

@app.route('/webhook/<project>', methods=['POST'])
def webhook(project):

    if project in PROJECTS:

        gitlabtoken = request.headers['X-Gitlab-Token']
        projectname = PROJECTS[PROJECTS.index(project)]
        projectpath = PATHS[PROJECTS.index(project)]
        projectdeployscript = DEPLOYMENTSCRIPT[PROJECTS.index(project)]

        if request.method == 'POST':
            if gitlabtoken == SECRET_TOKEN:

                os.system("cd {} && {}".format(projectpath, projectdeployscript))

                return jsonify({
                    'project': "{}".format(projectname),
                    'path': "{}".format(projectpath),
                    'script': "{}".format(projectdeployscript)
                }), 200
            else:
                return jsonify({'message': 'bad token'}), 401
        else:
            return jsonify({'message': 'method not allowed'}), 401
    else:
        return jsonify({'message': 'project not listed'}), 401

if __name__ == '__main__':
    app.run(port="{}".format(PORT))